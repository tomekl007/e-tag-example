package com.logicbig.example;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;
import java.util.Map;

@RestController
public class TheController {

    private final Map<String, Person> cache = new HashMap<>();

    @ResponseBody
    @RequestMapping(value = "/api/resource/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Person> getPerson(@PathVariable String id,
                                            WebRequest swr) {
        Person person = cache.get(id);
        if (person == null) {
            return ResponseEntity.notFound().build();
        } else {
            String etag = calculateEtag(person);

            if (swr.checkNotModified(calculateEtag(person))) {
                return null;
            }

            return ResponseEntity
                    .ok()
                    .eTag(etag)
                    .body(person);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/api/resource/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Person> modifyPerson(
            @PathVariable String id,
            @RequestBody PersonChangeSurnameRequest personChangeSurnameRequest) {
        Person person = cache.getOrDefault(id, createPerson(personChangeSurnameRequest));

        person.setFirstName(personChangeSurnameRequest.getFirstName());
        person.setSurname(personChangeSurnameRequest.getSurname());
        cache.put(id, person);
        String etag = calculateEtag(person);
        return ResponseEntity
                .ok()
                .eTag(etag)
                .body(person);


    }

    private Person createPerson(@RequestBody PersonChangeSurnameRequest personChangeSurnameRequest) {
        return new Person(personChangeSurnameRequest.getFirstName(),
                personChangeSurnameRequest.getSurname()
        );
    }

    private String calculateEtag(Person person) {
        return String.valueOf(person.getFirstName().hashCode() + person.getSurname().hashCode());
    }

}